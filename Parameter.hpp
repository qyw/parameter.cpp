//
// Created by kyb on 2016-09-12.
//

#pragma once


// note you can't use comma in last argument, it would require more complex macro
#define ALL_CV_QUALIFIED_FUNCTIONS( returnType, functionName, functionArgs, functionBody ) \
                   returnType functionName functionArgs                functionBody \
    volatile       returnType functionName functionArgs volatile       functionBody \
    const          returnType functionName functionArgs const          functionBody \
    const volatile returnType functionName functionArgs volatile const functionBody

#define CONST_QUALIFIED_FUNCTIONS( returnType, functionName, functionArgs, functionBody ) \
    const          returnType functionName functionArgs const          functionBody \
    const volatile returnType functionName functionArgs volatile const functionBody


template <typename T>
class Parameter //: public Object
{
public:
	typedef T value_type;
	typedef Parameter<T> Type;

	template< typename return_t>
	struct Function{
		return_t operator()() { return return_t(); }
		return_t operator()() const { return return_t(3);}
	};
	Function<T> function;

protected:
	T value_;
	//todo std::string name_;

public:
	Parameter(T&& val){
		value(val);
	}
	Parameter(const T& val){
		value(val);
	}
	Parameter() = default;
	virtual ~Parameter(){}
	
	// Get
//	virtual T& get()                                  {  return value_; }
//	virtual const T& get() const                      {  return value_; }
//	virtual volatile T& get() volatile                {  return value_; }
//	virtual const volatile T& get() const volatile    {  return value_; }
	ALL_CV_QUALIFIED_FUNCTIONS(
			T&, get, (), { return value_;}
	)

	// Const get()
//	const T& cget() const                     {  return get(); }
//	const volatile T& cget() const volatile   {  return get(); }
	CONST_QUALIFIED_FUNCTIONS(
			T&, cget, (), { return get();}
	)

	// Alias
	/*T& value()                                 { return get();	}
	volatile T& value() volatile               { return get();	}
	const T& value() const                     { return get();	}
	const volatile T& value() volatile const   { return get();	}*/
	ALL_CV_QUALIFIED_FUNCTIONS(
			T&, value, (), { return get();}
	)

	// Const alias
	/*const T& cvalue() const                    { return value(); }
	const volatile T& cvalue() volatile const  { return value(); }*/
	CONST_QUALIFIED_FUNCTIONS(
			T&, cvalue, (), { return cget();}
	)

	operator T()                {  return get(); }
	operator T() const          {  return get(); }
	operator T() volatile       {  return get(); }
	operator T() const volatile {  return get(); }
	/*ALL_CV_QUALIFIED_FUNCTIONS(
			, operator T, (), { return get();}
	)*/
	
	// Set
	virtual Type* set(T && val)          {  value_ = val; return this; }
	virtual Type* set(T const &val)      {  value_ = val; return this; }

	Type* value(T const &val)    {	return set(val); }
	Type* value(T && val)        {  return set(val); }

	virtual Type* operator=(T const &val){  return set(val); }
	virtual Type* operator=(T && val)    {  return set(val); }

	/*friend operator Type()(const T& val);
	friend operator Type()(T && val);*/
};

//TODO class NamedParameter;
