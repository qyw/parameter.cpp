#include <iostream>
#include "Parameter.hpp"

using namespace std;

struct Student
{
	struct A : Parameter<float>
	{
		using Parameter<float>::operator=;  // Need to call A::set() but not Parameter<>::set()
//		using Parameter<float>::get;
		using Parameter<float>::set;
		A* set(float && val){ value_=val+1; return this; }
		A* set(const float & val){ value_=val+1; return this; }
//		float & get(){  return value_; }
//		Age & set(float const &f){ value_=f*2; return *this; }
	} age;

	/*TODO
	Parameter<float> age{
		//get(){return value_}  //Inherited
	    set(const float& val){ value_= val>0 && val<200 ? val :; return *this; }  //TODO return *this опустить
	}
	*/
};

int main()
{
	Parameter<int> par(2);
	par = 33;
	cout<<par<<endl;
	cout<< *(par=44)<<" "<<*(par=32)<<endl;
	cout<<endl;

	Parameter<int> par2=22;
	cout<<par2<<endl;
	cout<<endl;

	Student student;
	student.age = float(2);  //what is return type?? Need decltype of student
	student.age.A::operator=(float(2));  //what is return type?? Need decltype of student
	cout << "student.age " << student.age <<"  "<< static_cast<int>(student.age) << endl;
	student.age.set(2);
	cout << "student.age " << student.age <<"  "<< static_cast<int>(student.age) << endl;
	cout<<endl;

	return 0;
}