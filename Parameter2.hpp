//
// Created by kyb on 2016-09-12.
//

#pragma once



template <typename T>
class Parameter //: public Object
{
public:
	typedef T value_type;
	typedef Parameter<T> Type;

protected:
	T value_;
	//todo std::string name_;

public:
	Parameter(T&& val){
		value(val);
	}
	Parameter(const T& val){
		value(val);
	}
	Parameter() = default;
	
	// Get
	virtual T& get()                                  {  return value_; }
	virtual const T& get() const                      {  return value_; }
	virtual volatile T& get() volatile                {  return value_; }
	virtual const volatile T& get() const volatile    {  return value_; }

	const T& cget() const                     {  return get(); }
	const volatile T& cget() const volatile   {  return get(); }

	T& value()                                 { return get();	}
	volatile T& value() volatile               { return get();	}
	const T& value() const                     { return get();	}
	const volatile T& value() volatile const   { return get();	}

	const T& cvalue() const                    { return value(); }
	const volatile T& cvalue() volatile const  { return value(); }

	operator T()                {  return get(); }
	operator T() const          {  return get(); }
	operator T() volatile       {  return get(); }
	operator T() const volatile {  return get(); }

	
	// Set
	virtual Type& set(T && val)          {  value_ = val; return *this; }
	virtual Type& set(T const &val)      {  value_ = val; return *this; }
	Type& value(T const &val)    {	return set(val); }
	Type& value(T && val)        {  return set(val); }

//	Type& operator=(T val)       {  return set(val); }
	Type& operator=(T const &val){  return set(val); }
	Type& operator=(T && val)    {  return set(val); }

//	friend operator Type()(const T& val);
//	friend operator Type()(T && val);
};

//TODO class NamedParameter;
